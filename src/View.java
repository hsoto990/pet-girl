
import java.awt.AWTException;
import java.awt.Button;
import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * Java 360 Sphere Image Viewer.
 * 
 * Resources: factory image:
 * https://i.ytimg.com/vi/Ifis9sSyPSY/maxresdefault.jpg park image:
 * http://www.mediavr.com/belmorepark1left.jpg
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
public class View extends Canvas implements MouseMotionListener {
	private static final long serialVersionUID = 1L;

	private BufferedImage sphereImage;
	private final BufferedImage offscreenImage;
	private int[] sphereImageBuffer;
	private final int[] offscreenImageBuffer;
	private double rayVecs[][][];
	private double[] asinTable;
	private double[] atan2Table;
	private static final double INV_PI = 1 / Math.PI;
	private static final double INV_2PI = 1 / (2 * Math.PI);
	private double targetRotationX, targetRotationY;
	private double currentRotationX, currentRotationY;
	private int mouseX, mouseY;

	private BufferStrategy bs;

	public View() {
		try {
			BufferedImage sphereTmpImage = ImageIO.read(getClass().getResourceAsStream("/res/Kitchen.png"));
			sphereImage = new BufferedImage(sphereTmpImage.getWidth(), sphereTmpImage.getHeight(),
					BufferedImage.TYPE_INT_RGB);
			sphereImage.getGraphics().drawImage(sphereTmpImage, 0, 0, null);
			sphereImageBuffer = ((DataBufferInt) sphereImage.getRaster().getDataBuffer()).getData();
		} catch (IOException ex) {
			System.exit(-1);
		}
		offscreenImage = new BufferedImage(800, 500, BufferedImage.TYPE_INT_RGB);
		offscreenImageBuffer = ((DataBufferInt) offscreenImage.getRaster().getDataBuffer()).getData();
		createRayVecs();
		precalculateAsinAtan2();
		addMouseMotionListener(this);
	}

	private void createRayVecs() {
		rayVecs = new double[800][500][3]; // x, y, z
		for (int y = 0; y < 500; y++) {
			for (int x = 0; x < 800; x++) {
				double vecX = x - 400;
				double vecY = y - 250;

				double invVecLength = 1 / Math.sqrt(Math.pow(vecX, 2) + Math.pow(vecY, 2) + 160000);
				rayVecs[x][y][0] = vecX * invVecLength;
				rayVecs[x][y][1] = vecY * invVecLength;
				rayVecs[x][y][2] = 400 * invVecLength;
			}
		}
	}

	private void precalculateAsinAtan2() {
		asinTable = new double[1000];
		atan2Table = new double[1000000];
		for (int i = 0; i < 1000; i++) {
			asinTable[i] = Math.asin((i - 500) * 0.002);
			for (int j = 0; j < 1000; j++) {
				double y = (i - 500) / 500d;
				double x = (j - 500) / 500d;
				atan2Table[i + j * 1000] = Math.atan2(y, x);
			}
		}
	}

	public void start() {
		createBufferStrategy(2);
		bs = getBufferStrategy();
		Graphics2D g = (Graphics2D) bs.getDrawGraphics();
		draw(g);
	}

	private void draw(Graphics2D g) {
		targetRotationX = (mouseY - 250) * 0.025 + 0.65;
		targetRotationY = (mouseX - 400) * 0.025 + 0.025;
		try {
			Robot r = new Robot();
			r.mouseMove(View.frame.getX() + 400, View.frame.getY() + 250);
		} catch (AWTException e1) {
		}
		if (targetRotationX != 0.0 | targetRotationY != 0.0) {
			currentRotationX += (targetRotationX / 2);
			currentRotationY -= (targetRotationY / 2);
			double sinRotationX = Math.sin(currentRotationX);
			double cosRotationX = Math.cos(currentRotationX);
			double sinRotationY = Math.sin(currentRotationY);
			double cosRotationY = Math.cos(currentRotationY);
			double tmpVecX, tmpVecY, tmpVecZ;
			for (int y = 0; y < 500; y++) {
				for (int x = 0; x < 800; x++) {
					double vecX = rayVecs[x][y][0];
					double vecY = rayVecs[x][y][1];
					double vecZ = rayVecs[x][y][2];
					// rotate x
					tmpVecZ = vecZ * cosRotationX - vecY * sinRotationX;
					tmpVecY = vecZ * sinRotationX + vecY * cosRotationX;
					vecZ = tmpVecZ;
					vecY = tmpVecY;
					// rotate y
					tmpVecZ = vecZ * cosRotationY - vecX * sinRotationY;
					tmpVecX = vecZ * sinRotationY + vecX * cosRotationY;
					vecZ = tmpVecZ;
					vecX = tmpVecX;
					int iX = (int) ((vecX + 1) * 500);
					int iY = (int) ((vecY + 1) * 500);
					int iZ = (int) ((vecZ + 1) * 500);
					// https://en.wikipedia.org/wiki/UV_mapping
					double u = 0.5 + (atan2Table[iZ + iX * 1000] * INV_2PI);
					double v = 0.5 - (asinTable[iY] * INV_PI);
					int tx = (int) (2000 * u);
					int ty = (int) (1000 * (1 - v));
					int color = sphereImageBuffer[ty * 2000 + tx];
					offscreenImageBuffer[y * 800 + x] = color;
				}
			}
			g.drawImage(offscreenImage, getWidth(), 0, -getWidth(), getHeight(), null);
			bs.show();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// do nothing
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		Graphics2D g = (Graphics2D) bs.getDrawGraphics();
		draw(g);
	}

	static JFrame frame = new JFrame();

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				View view = new View();
				frame.setTitle("Java 360 Sphere Image Viewer");
				frame.setBounds(100, 50, 800, 500);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
				frame.setLocationRelativeTo(null);
				frame.getContentPane().add(view);
				frame.setResizable(false);
				frame.setVisible(true);
				view.requestFocus();
				view.start();
				// Create a new blank cursor.
				frame.getContentPane().setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
						new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "blank cursor"));
				Button sa = new Button("sa");
				sa.setBounds(10, 10, 10, 10);
				frame.add(sa);
			}
		});
	}

}
