import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class PetGirl extends JFrame {
	private static final long serialVersionUID = 1L;

	private static byte dias = 1;
	private static byte hora = 7;
	private static byte minu = 0;
	private static JPanel contentPane;
	private static JLabel lblNewLabel;
	private static JLabel label_12;
	private static JProgressBar sleepBar;
	private static JProgressBar sweetBar;
	private static JProgressBar sanityBar;
	private static JProgressBar socialBar;
	private static JProgressBar funBar;
	private static JProgressBar toiletBar;
	private static JProgressBar cleanBar;
	private static JProgressBar hungerBar;
	private static JProgressBar thirstBar;
	private static JTabbedPane tabbedPane;
	private static JPanel actioning;
	private static JLabel label_16;
	private static JLabel label_17;
	private static String stdir = "/home/ceibal/Escritorio/Carpeta sin título 2/";

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public PetGirl() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 801, 530);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(157, 0, 339, 21);
		contentPane.add(panel);

		toiletBar = new JProgressBar();
		toiletBar.setForeground(new Color(173, 255, 47));
		toiletBar.setValue(60);
		toiletBar.setBounds(10, 140, 148, 15);
		contentPane.add(toiletBar);

		cleanBar = new JProgressBar();
		cleanBar.setForeground(new Color(173, 255, 47));
		cleanBar.setValue(60);
		cleanBar.setBounds(10, 170, 148, 15);
		contentPane.add(cleanBar);

		hungerBar = new JProgressBar();
		hungerBar.setForeground(new Color(173, 255, 47));
		hungerBar.setValue(60);
		hungerBar.setBounds(10, 200, 148, 15);
		contentPane.add(hungerBar);

		thirstBar = new JProgressBar();
		thirstBar.setForeground(new Color(173, 255, 47));
		thirstBar.setValue(60);
		thirstBar.setBounds(10, 230, 148, 15);
		contentPane.add(thirstBar);

		funBar = new JProgressBar();
		funBar.setForeground(new Color(173, 255, 47));
		funBar.setValue(60);
		funBar.setBounds(10, 260, 148, 15);
		contentPane.add(funBar);

		socialBar = new JProgressBar();
		socialBar.setForeground(new Color(173, 255, 47));
		socialBar.setValue(60);
		socialBar.setBounds(10, 290, 148, 15);
		contentPane.add(socialBar);

		sanityBar = new JProgressBar();
		sanityBar.setForeground(new Color(173, 255, 47));
		sanityBar.setValue(60);
		sanityBar.setBounds(10, 320, 148, 15);
		contentPane.add(sanityBar);

		sweetBar = new JProgressBar();
		sweetBar.setForeground(new Color(173, 255, 47));
		sweetBar.setValue(60);
		sweetBar.setBounds(10, 350, 148, 15);
		contentPane.add(sweetBar);

		sleepBar = new JProgressBar();
		sleepBar.setValue(100);
		sleepBar.setForeground(new Color(173, 255, 47));
		sleepBar.setBounds(10, 380, 148, 15);
		contentPane.add(sleepBar);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(170, 0, 638, 505);
		contentPane.add(tabbedPane);

		JPanel hallroom = new JPanel();
		tabbedPane.addTab("-", null, hallroom, null);
		hallroom.setLayout(null);

		JButton btnCocina = new JButton("Cocina");
		btnCocina.setBounds(375, 292, 100, 25);
		hallroom.add(btnCocina);

		JButton btnHabitacin = new JButton("Habitación");
		btnHabitacin.setBounds(177, 276, 100, 25);
		hallroom.add(btnHabitacin);

		JButton btnBao = new JButton("Baño");
		btnBao.setBounds(22, 292, 100, 25);
		hallroom.add(btnBao);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(239, 441, 100, 25);
		hallroom.add(btnSalir);

		
		JButton btnSentar_1 = new JButton("Sentar");

		btnSentar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				sleepBar.setValue(sleepBar.getValue() + 10);
				funBar.setValue(funBar.getValue() - 5);

			}
		});
		btnSentar_1.setBounds(494, 376, 100, 25);
		hallroom.add(btnSentar_1);

		JLabel label_8 = new JLabel("");
		label_8.setBounds(0, 0, 640, 480);
		label_8.setIcon(new ImageIcon(stdir+"Scenes/Hall.png"));
		hallroom.add(label_8);

		JPanel bathroom = new JPanel();
		tabbedPane.addTab("-", null, bathroom, null);
		bathroom.setLayout(null);

		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(256, 441, 100, 25);
		bathroom.add(btnVolver);

		JButton btnBaar = new JButton("Bañar");
		btnBaar.setBounds(97, 265, 100, 25);
		bathroom.add(btnBaar);

		JButton btnHacerUsar = new JButton("Hacer usar");
		btnHacerUsar.setBounds(470, 323, 100, 25);
		bathroom.add(btnHacerUsar);

		JButton btnPeinar = new JButton("Peinar");
		btnPeinar.setBounds(434, 40, 100, 25);
		bathroom.add(btnPeinar);
		adistener(btnPeinar, "Bathroom", "Mirroring", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.2);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(stdir + "Scenes/Bathroom.png"));
		label.setBounds(0, 0, 640, 480);
		bathroom.add(label);

		JPanel habitacion = new JPanel();
		tabbedPane.addTab("-", null, habitacion, null);
		habitacion.setLayout(null);

		JButton btnAcostar = new JButton("Acostar");
		btnAcostar.setBounds(285, 266, 100, 25);
		habitacion.add(btnAcostar);

		JButton btnVestir = new JButton("Vestir");
		btnVestir.setBounds(428, 119, 100, 25);
		habitacion.add(btnVestir);

		JButton btnVolver_1 = new JButton("Volver");
		btnVolver_1.setBounds(133, 441, 100, 25);
		habitacion.add(btnVolver_1);

		JLabel label_1 = new JLabel("");
		label_1.setBounds(0, 0, 640, 480);
		label_1.setIcon(new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título 2/Scenes/Bedroom.png"));
		habitacion.add(label_1);

		JPanel plaza = new JPanel();
		tabbedPane.addTab("-", null, plaza, null);
		plaza.setLayout(null);

		JButton btnMontar = new JButton("Montar");
		btnMontar.setBounds(189, 404, 100, 25);
		plaza.add(btnMontar);

		JButton button = new JButton("Montar");

		button.setBounds(45, 354, 100, 25);
		plaza.add(button);

		JButton btnDeslizar = new JButton("Deslizar");
		btnDeslizar.setBounds(141, 252, 100, 25);
		plaza.add(btnDeslizar);

		JButton btnAmacar = new JButton("Amacar");
		btnAmacar.setBounds(466, 354, 100, 25);
		plaza.add(btnAmacar);

		JButton btnVolver_2 = new JButton("Volver");
		btnVolver_2.setBounds(314, 441, 100, 25);
		plaza.add(btnVolver_2);

		JButton btnFootball = new JButton("Football");
		btnFootball.setBounds(346, 221, 100, 25);
		plaza.add(btnFootball);

		JLabel label_2 = new JLabel("");
		label_2.setBounds(0, -34, 640, 480);
		label_2.setIcon(new ImageIcon(stdir + "Scenes/Field.png"));
		plaza.add(label_2);

		JPanel football = new JPanel();
		tabbedPane.addTab("-", null, football, null);
		football.setLayout(null);

		JButton btnLanzar = new JButton("Patear");
		btnLanzar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				funBar.setValue(funBar.getValue() + 10);
				sleepBar.setValue(sleepBar.getValue() - 5);
				sweetBar.setValue(sweetBar.getValue() + 2);

			}
		});
		btnLanzar.setBounds(275, 427, 100, 25);
		football.add(btnLanzar);

		JButton btnVolver_3 = new JButton("Volver");
		btnVolver_3.setBounds(12, 441, 100, 25);
		football.add(btnVolver_3);

		JLabel label_3 = new JLabel("");
		label_3.setBounds(-3, 5, 640, 480);
		label_3.setIcon(new ImageIcon(stdir + "Scenes/Football.png"));
		football.add(label_3);

		JPanel gameroom = new JPanel();
		tabbedPane.addTab("-", null, gameroom, null);
		gameroom.setLayout(null);

		JButton btnJugar1 = new JButton("Jugar");
		btnJugar1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funBar.setValue(funBar.getValue() + 10);

			}
		});
		btnJugar1.setBounds(50, 245, 75, 25);
		gameroom.add(btnJugar1);

		JButton btnJugar2 = new JButton("Jugar");
		btnJugar2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funBar.setValue(funBar.getValue() + 10);

			}
		});
		btnJugar2.setBounds(120, 261, 75, 25);
		gameroom.add(btnJugar2);

		JButton btnJugar3 = new JButton("Jugar");
		btnJugar3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funBar.setValue(funBar.getValue() + 10);

			}
		});
		btnJugar3.setBounds(197, 275, 75, 25);
		gameroom.add(btnJugar3);

		JButton btnJugar4 = new JButton("Jugar");
		btnJugar4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funBar.setValue(funBar.getValue() + 10);

			}
		});
		btnJugar4.setBounds(309, 297, 75, 25);
		gameroom.add(btnJugar4);

		JButton btnJugar5 = new JButton("Jugar");
		btnJugar5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funBar.setValue(funBar.getValue() + 10);

			}
		});
		btnJugar5.setBounds(445, 333, 75, 25);
		gameroom.add(btnJugar5);

		JButton btnVolver_4 = new JButton("Volver");
		btnVolver_4.setBounds(12, 441, 100, 25);
		gameroom.add(btnVolver_4);

		JButton btnDanceGo = new JButton("Dance & Go");

		btnDanceGo.setBounds(12, 120, 100, 25);
		gameroom.add(btnDanceGo);
		JLabel label_4 = new JLabel("");
		label_4.setBounds(0, 38, 640, 480);
		label_4.setIcon(new ImageIcon(stdir + "Scenes/Gameroom.png"));
		gameroom.add(label_4);

		JPanel cineroom = new JPanel();
		tabbedPane.addTab("-", null, cineroom, null);
		cineroom.setLayout(null);

		JButton btnSentarse = new JButton("Sentarse");
		btnSentarse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				funBar.setValue(funBar.getValue() + 5);
				sweetBar.setValue(sweetBar.getValue() + 10);
			}
		});
		btnSentarse.setBounds(433, 357, 100, 25);
		cineroom.add(btnSentarse);

		JButton btnCambiar = new JButton("Cambiar");
		btnCambiar.setBounds(298, 323, 100, 25);
		cineroom.add(btnCambiar);

		JButton btnBailar = new JButton("Bailar");
		btnBailar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				funBar.setValue(funBar.getValue() + 10);
				sleepBar.setValue(sleepBar.getValue() - 10);
				sweetBar.setValue(sweetBar.getValue() + 10);
			}
		});
		btnBailar.setBounds(167, 264, 100, 25);
		cineroom.add(btnBailar);

		JButton btnVolver_5 = new JButton("Volver");
		btnVolver_5.setBounds(66, 441, 100, 25);
		cineroom.add(btnVolver_5);

		JLabel label_5 = new JLabel("");
		label_5.setBounds(-3, 5, 640, 480);
		label_5.setIcon(new ImageIcon(stdir + "Scenes/Cineroom.png"));
		cineroom.add(label_5);

		JPanel teatable = new JPanel();
		tabbedPane.addTab("-", null, teatable, null);
		teatable.setLayout(null);

		JButton btnServirT = new JButton("Servir té");
		btnServirT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				thirstBar.setValue(100);
				sweetBar.setValue(sweetBar.getValue() + 30);
			}
		});
		btnServirT.setBounds(260, 215, 100, 25);
		teatable.add(btnServirT);

		JButton btnVolver_6 = new JButton("Volver");
		btnVolver_6.setBounds(276, 441, 100, 25);
		teatable.add(btnVolver_6);

		JLabel label_6 = new JLabel("");
		label_6.setBounds(-3, 5, 640, 480);
		label_6.setIcon(new ImageIcon(stdir + "Scenes/TeaTable.png"));
		teatable.add(label_6);

		JPanel garden = new JPanel();
		tabbedPane.addTab("-", null, garden, null);
		garden.setLayout(null);

		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(0, 165, 100, 25);
		garden.add(btnEntrar);

		JButton btnCuidar = new JButton("Cuidar");
		btnCuidar.setBounds(203, 226, 100, 25);
		garden.add(btnCuidar);

		JButton btnMercado = new JButton("Mercado");
		btnMercado.setBounds(121, 441, 100, 25);
		garden.add(btnMercado);

		JButton btnMesita = new JButton("Mesita");
		btnMesita.setBounds(-3, 299, 100, 25);
		garden.add(btnMesita);

		JLabel label_7 = new JLabel("");
		label_7.setBounds(-3, 5, 640, 480);
		label_7.setIcon(new ImageIcon(stdir + "Scenes/Garden.png"));
		garden.add(label_7);

		JPanel kitchen = new JPanel();
		tabbedPane.addTab("-", null, kitchen, null);
		kitchen.setLayout(null);

		JButton btnCocinar = new JButton("Cocinar");
		btnCocinar.setBounds(256, 279, 100, 25);
		kitchen.add(btnCocinar);

		JButton btnTomar = new JButton("Tomar");
		btnTomar.setBounds(508, 279, 100, 25);
		kitchen.add(btnTomar);

		JButton btnVolver_7 = new JButton("Volver");
		btnVolver_7.setBounds(256, 441, 100, 25);
		kitchen.add(btnVolver_7);

		JButton btnNewButton = new JButton("Laboratorio");
		btnNewButton.setBounds(-3, 297, 100, 25);
		kitchen.add(btnNewButton);

		JLabel label_15 = new JLabel("");
		label_15.setBounds(0, 0, 640, 480);
		kitchen.add(label_15);

		JLabel label_9 = new JLabel("");
		label_9.setBounds(0, 0, 640, 480);
		label_9.setIcon(new ImageIcon(stdir + "Scenes/Kitchen.png"));
		kitchen.add(label_9);

		JPanel laboratory = new JPanel();
		tabbedPane.addTab("-", null, laboratory, null);
		laboratory.setLayout(null);

		JButton btnPreparar = new JButton("Preparar");
		btnPreparar.setBounds(262, 261, 100, 25);
		laboratory.add(btnPreparar);

		JButton btnVolver_8 = new JButton("Volver");
		btnVolver_8.setBounds(262, 441, 100, 25);
		laboratory.add(btnVolver_8);

		JLabel label_10 = new JLabel("");
		label_10.setBounds(-3, 5, 640, 480);
		label_10.setIcon(new ImageIcon(stdir + "Scenes/Laboratory.png"));
		laboratory.add(label_10);

		JPanel market = new JPanel();
		tabbedPane.addTab("-", null, market, null);
		market.setLayout(null);

		JButton btnVolver_9 = new JButton("Volver");
		btnVolver_9.setBounds(309, 441, 100, 25);
		market.add(btnVolver_9);

		JButton btnComprar = new JButton("Comprar");
		btnComprar.setBounds(134, 255, 100, 25);
		market.add(btnComprar);

		JButton btnParque = new JButton("Parque");
		btnParque.setBounds(374, 206, 100, 25);
		market.add(btnParque);

		JButton btnArcade = new JButton("Arcade");
		btnArcade.setBounds(533, 304, 100, 25);
		market.add(btnArcade);

		JLabel label_11 = new JLabel("");
		label_11.setBounds(-3, 5, 640, 480);
		label_11.setIcon(new ImageIcon(stdir + "Scenes/Market.png"));
		market.add(label_11);

		lblNewLabel = new JLabel("Día 1");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 18));
		lblNewLabel.setBounds(55, 415, 53, 15);
		contentPane.add(lblNewLabel);

		label_12 = new JLabel("7:00");
		label_12.setFont(new Font("Dialog", Font.BOLD, 18));
		label_12.setBounds(63, 456, 53, 15);
		contentPane.add(label_12);

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				try {
					Robot r = new Robot();
					r.mouseMove(PetGirl.this.getX()+100,PetGirl.this.getY()+100);
									} catch (AWTException e) {
				}
			}
		});
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				timerendo();
			}
		});
		lblNewLabel_1.setIcon(new ImageIcon(stdir + "Scenes/Face2.jpg"));
		lblNewLabel_1.setBounds(27, 4, 131, 119);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Baño");
		lblNewLabel_2.setBounds(22, 128, 53, 15);
		contentPane.add(lblNewLabel_2);

		JLabel lblLimpieza = new JLabel("Limpieza");
		lblLimpieza.setBounds(20, 155, 53, 15);
		contentPane.add(lblLimpieza);

		JLabel lblHambre = new JLabel("Hambre");
		lblHambre.setBounds(20, 187, 53, 15);
		contentPane.add(lblHambre);

		JLabel lblSed = new JLabel("Sed");
		lblSed.setBounds(20, 217, 53, 15);
		contentPane.add(lblSed);

		JLabel lblDiversin = new JLabel("Diversión");
		lblDiversin.setBounds(20, 246, 53, 15);
		contentPane.add(lblDiversin);

		JLabel lblSocial = new JLabel("Social");
		lblSocial.setBounds(20, 276, 53, 15);
		contentPane.add(lblSocial);

		JLabel lblCordura = new JLabel("Cordura");
		lblCordura.setBounds(20, 304, 53, 15);
		contentPane.add(lblCordura);

		JLabel lblFelicidad = new JLabel("Felicidad");
		lblFelicidad.setBounds(20, 336, 53, 15);
		contentPane.add(lblFelicidad);

		JLabel lblSueo = new JLabel("Sueño");
		lblSueo.setBounds(20, 366, 53, 15);
		contentPane.add(lblSueo);

		adistviaj(btnSalir, garden);
		adistviaj(btnBao, bathroom);
		adistviaj(btnHabitacin, habitacion);
		adistviaj(btnCocina, kitchen);
		adistviaj(btnVolver, hallroom);
		adistviaj(btnVolver_4, market);
		adistviaj(btnArcade, gameroom);
		adistviaj(btnMercado, market);
		adistviaj(btnMesita, teatable);
		adistviaj(btnVolver_5, gameroom);
		adistviaj(btnVolver_3, plaza);
		adistviaj(btnVolver_6, garden);
		adistviaj(btnVolver_8, kitchen);
		adistviaj(btnDanceGo, cineroom);
		adistviaj(btnEntrar, hallroom);
		adistviaj(btnNewButton, laboratory);
		adistviaj(btnVolver_9, garden);
		adistviaj(btnVolver_7, hallroom);
		adistviaj(btnVolver_2, market);
		adistviaj(btnParque, plaza);
		adistviaj(btnVolver_1, hallroom);
		adistviaj(btnFootball, football);

		// ------------------Acciones-------------------------------------------------------------------
		adistener(btnBaar, "Bathroom", "Bathing", 0, 0, 10, 0, 10, 0, 100, 0, 0, 0.5);
		adistener(btnHacerUsar, "Bathroom", "toileting", 0, 0, 0, 0, 0, 100, -10, 0, 0, 0.2);

		adistener(btnAcostar, "Bedroom", "Sleeping", 100, 0, 0, 0, 0, 0, 0, 0, 0, 8);
		adistener(btnVestir, "Bedroom", "Dressing", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.2);

		adistener(btnTomar, "Kitchen", "Drinking", 0, 0, 0, 0, 0, 0, 0, 0, 100, 0.1);
		adistener(btnCocinar, "Kitchen", "Eating", 0, 0, 0, 0, 0, 0, 0, 100, 0, 0.4);

		adistener(button, "Field", "MontarG", -5, 0, 0, 0, 10, 0, 0, 0, 0, 0.2);
		adistener(btnAmacar, "Field", "Amacar", 0, 0, 0, 0, 10, 0, 0, 0, 0, 0.2);
		adistener(btnMontar, "Field", "MontarH", -5, 0, 0, 0, 10, 0, 0, 0, 0, 0.2);
		adistener(btnDeslizar, "Field", "Deslizar", -10, 0, 0, 0, 20, 0, 0, 0, 0, 0.2);

		actioning = new JPanel();
		tabbedPane.addTab("New tab", null, actioning, null);
		actioning.setLayout(null);

		label_17 = new JLabel("");
		label_17.setBounds(0, 0, 640, 480);
		actioning.add(label_17);

		label_16 = new JLabel("");
		label_16.setBounds(0, 0, 640, 480);
		actioning.add(label_16);
	}

	static void addProgressBar(JProgressBar Barradera, int altura, int valor) {
		Barradera.setForeground(new Color(173, 255, 47));
		Barradera.setValue(valor);
		Barradera.setBounds(10, altura, 148, 15);
		contentPane.add(Barradera);
	}

	static void UpdatoBarColor(JProgressBar Barradera) {
		if (Barradera.getValue() < 15)
			Barradera.setForeground(Color.RED);
		if (Barradera.getValue() < 30)
			Barradera.setForeground(Color.ORANGE);
		else if (Barradera.getValue() < 50)
			Barradera.setForeground(Color.YELLOW);
		else if (Barradera.getValue() < 75)
			Barradera.setForeground(new Color(173, 255, 47));
		else
			Barradera.setForeground(new Color(0, 255, 0));
	}

	static void UpdatoAllBars() {
		UpdatoBarColor(sleepBar);
		UpdatoBarColor(sweetBar);
		UpdatoBarColor(sanityBar);
		UpdatoBarColor(socialBar);
		UpdatoBarColor(funBar);
		UpdatoBarColor(toiletBar);
		UpdatoBarColor(cleanBar);
		UpdatoBarColor(hungerBar);
		UpdatoBarColor(thirstBar);
	}

	static int veltime = 300;

	static void timerendo() {

		new Thread() {
			public void run() {
				while (true) {
					for (byte b = 0; b < 30; b++) {
						try {
							Thread.sleep(veltime);
						} catch (InterruptedException e) {
						}
						PetGirl.minu++;
						if (PetGirl.minu == 60) {
							PetGirl.minu = 0;
							PetGirl.hora++;
							if (PetGirl.hora == 24) {
								PetGirl.hora = 0;
								PetGirl.dias++;
								PetGirl.lblNewLabel.setText("Día " + PetGirl.dias);
							}
						}
						if (PetGirl.minu < 10)
							PetGirl.label_12.setText(PetGirl.hora + ":0" + PetGirl.minu);
						else
							PetGirl.label_12.setText(PetGirl.hora + ":" + PetGirl.minu);
					}
					altValue(sleepBar, - 1);
					altValue(cleanBar, - 1);
					altValue(hungerBar, - 2);
					altValue(toiletBar, - 2);
					altValue(thirstBar, - 3);
					altValue(funBar, - 3);
					UpdatoAllBars();
				}
			}
		}.start();
	}

	static void adistener(JButton buttoner, String room, String pose, int sleepVar, int sweetVar, int sanityVar,
			int socialVar, int funVar, int toiletVar, int cleanVar, int hungerVar, int thirstVar, double mija) {
		buttoner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				label_16.setIcon(new ImageIcon(stdir + "Scenes/" + room + ".png"));
				label_17.setIcon(new ImageIcon(stdir + "Scenes/" + room + "/" + pose + ".png"));
				int indax = tabbedPane.getSelectedIndex();
				tabbedPane.setSelectedComponent(actioning);
				new Thread() {
					public void run() {
						try {
							Thread.sleep((long) (veltime * 60 * mija));
							altValue(sleepBar, sleepVar);
							altValue(sweetBar, sweetVar);
							altValue(sanityBar, sanityVar);
							altValue(socialBar, socialVar);
							altValue(funBar, funVar);
							altValue(toiletBar, toiletVar);
							altValue(cleanBar, cleanVar);
							altValue(hungerBar, hungerVar);
							altValue(thirstBar, thirstVar);
							UpdatoAllBars();
							tabbedPane.setSelectedIndex(indax);
						} catch (InterruptedException e) {
						}
					}
				}.start();
			}
		});
	}

	static void altValue(JProgressBar asaBar, int valor) {
		asaBar.setValue(asaBar.getValue() + valor);
	}

	static void adistviaj(JButton buttoner, JPanel destin) {
		buttoner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabbedPane.setSelectedComponent(destin);
			}
		});
	}
}
