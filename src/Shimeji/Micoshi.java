package Shimeji;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Micoshi extends JFrame {
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Micoshi frame = new Micoshi();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	final int maxVestido = 1;
	final int maxPeinado = 2;
	static int actualVestido = 0;
	static int actualPeinado = 0;

	String SOURCEFOLDER = "/home/ceibal/Escritorio/Carpeta sin título 2/Hab/aassa/";

	JToggleButton btnLamp = new JToggleButton("Lamp");

	static JButton btnVestido = new JButton("Vestido");
	static JButton btnPeinado = new JButton("Peinado");

	JLabel lbl_Cena = new JLabel("");
	JLabel lblMesa = new JLabel("");

	JButton btnSientate = new JButton("Sientate");

	JLabel lblBoquita = new JLabel("");

	JLabel DressDressed = new JLabel("");
	JLabel Hairstile = new JLabel("");

	JLabel EmotionFaceEyeBrown = new JLabel("");
	JLabel EmotionFaceEye = new JLabel("");
	JLabel EmotionFaceMouth = new JLabel("");
	JLabel EmotionFaceSleepy = new JLabel("");
	JLabel EmotionFaceBlush = new JLabel("");

	JLabel lblCuerpo = new JLabel("");

	public static String inMano = "Index";
	public static String lugaPlace = "Inicio";

	JLabel label_IMC = new JLabel("IMC=24.00");
	JLabel lblDucha = new JLabel("");
	JLabel btnEsponja = new JLabel("");
	JLabel label = new JLabel("");
	double IMCValue = 24;
	static double intimityProgress = 255;
	static boolean mousePressed = false;
	JLabel btnHall = new JLabel("");
	private JLabel label_6;
	public static String Pose = "De pie";
	private static JProgressBar hungerBar = new JProgressBar();
	private static JProgressBar sleepBar = new JProgressBar();
	private static JProgressBar cleanBar = new JProgressBar();
	private static JProgressBar funBar = new JProgressBar();

	JLabel hungerLabel = new JLabel("Estomago Vacío");
	JLabel sleepLabel = new JLabel("Despierta");
	JLabel cleanLabel = new JLabel("Huele bien");
	JLabel funLabel = new JLabel("Está felíz");

	void confBarLabel(JLabel LabelVar, int y) {
		LabelVar.setBounds(0, y * 20, 100, 20);
		LabelVar.setOpaque(true);
		contentPane.add(LabelVar);
	}

	// ------EMOTION FACE COMPONENTS-----------------------------------------------

	public Micoshi() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(344, 0, 462, 552);
		contentPane = new JPanel();

		confgburing(hungerBar, 5, 20, 0);
		confgburing(sleepBar, 20, 20, 24);
		confgburing(cleanBar, 35, 20, 32);
		confgburing(funBar, 50, 20, 64);
		hungerBar.setMaximum(1000);

		confBarLabel(hungerLabel, 2);
		confBarLabel(sleepLabel, 3);
		confBarLabel(cleanLabel, 4);
		confBarLabel(funLabel, 5);
		confBarLabel(label_IMC, 6);
		btnPeinado.setVisible(false);

		btnPeinado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (actualPeinado == maxPeinado)
					actualPeinado = 0;
				else
					actualPeinado++;
				Hairstile.setIcon(new ImageIcon(SOURCEFOLDER + "Peinados/" + actualPeinado + ".png"));
			}
		});
		btnPeinado.setBounds(33, 148, 100, 25);
		contentPane.add(btnPeinado);
		// ------EMOTION FACE COMPONENTS-----------------------------------------------
		ConfFaceLabel(EmotionFaceEyeBrown, "Eyebrown/Cute");
		ConfFaceLabel(EmotionFaceEye, "Eyes/Cute");
		ConfFaceLabel(EmotionFaceMouth, "Mouth/5_Neutral");
		// Others
		ConfFaceLabel(EmotionFaceSleepy, "Others/null");
		ConfFaceLabel(EmotionFaceBlush, "Others/null");
		// ----------------------------------------------------------------------------

		// ------BOTONES DE CONTROL TEMPORAL---------------------------
		button_1 = new JButton(">");
		button_1.setVisible(false);
		buttonAltVeltime(button_1, 1000);
		button_1.setBounds(192, 2, 21, 25);
		contentPane.add(button_1);
		button_2 = new JButton(">>");
		button_2.setVisible(false);
		buttonAltVeltime(button_2, 100);
		button_2.setBounds(226, 2, 21, 25);
		contentPane.add(button_2);
		JButton button = new JButton(">>>");
		button.setVisible(false);
		buttonAltVeltime(button, 10);
		button.setBounds(259, 2, 21, 25);
		contentPane.add(button);
		// ------------------------------------------------------------

		lblCuerpo.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				if (!(lugaPlace.equals("Index")|lugaPlace.equals("Shop")) &inMano == "Mano") {
					try {
						BufferedImage icomand = ImageIO.read(getClass().getResourceAsStream("/res2/hotscale.png"));
						if (arg0.getX() < 300) {
							int toucheda = new Color(icomand.getRGB(arg0.getX(), arg0.getY() - Hairstile.getY()))
									.getBlue();

							if (toucheda == 0)
								contentPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
							else if (toucheda > intimityProgress + 10)
								petGraphicReaction("Corazon", "Happy", "Closed", "6_Pleased", 0, 3);
							else if (toucheda > intimityProgress - 30)
								petGraphicReaction("Estrella", "Cute", "Cute", "5_Neutral", -0.05, 0);
							else if (toucheda > intimityProgress - 50)
								petGraphicReaction("Estrella", "Cute", "Cute", "4_Disgusted", -0.05, 0);
							else if (toucheda > intimityProgress - 100)
								petGraphicReaction("Advertencia", "Angry", "Cute", "3_LittleAngry", 0.1, -3);
							else if (toucheda > intimityProgress - 150)
								petGraphicReaction("Advertencia", "Angry", "Cute", "2_SomeAngry", 0.1, -3);
							else
								petGraphicReaction("Pudrete", "Angry", "Angry", "1_Angry", 0.8, -15);

							updateIntimity();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			public void mouseReleased(MouseEvent arg0) {
				if (inMano == "Pet")
					contentPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
		});
		setContentPane(contentPane);
		contentPane.setLayout(null);
		lblDucha.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				contentPane.repaint();
			}
		});

		lbl_Cena.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lbl_Cena.setIcon(new ImageIcon(SOURCEFOLDER + "Sopa.png"));

				if (inMano == "Chopstick") {
					inMano = "Chopstick_food";
					changeCursor("Food/Chopstick_food", 0, 0);
				} else if (inMano == "Chopstick_food") {
					inMano = "Chopstick";
					changeCursor("Food/Chopstick", 0, 0);
				}
			}
		});
		lbl_Cena.setIcon(new ImageIcon(SOURCEFOLDER + "Sopa.png"));
		lbl_Cena.setBounds(125, 275, 86, 53);
		lbl_Cena.setVisible(false);
		btnVestido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (actualVestido == maxVestido)
					actualVestido = 0;
				else
					actualVestido++;
				DressDressed.setIcon(new ImageIcon(SOURCEFOLDER + "Dresses/" + actualVestido + ".png"));
			}
		});
		btnVestido.setBounds(218, 243, 100, 25);
		contentPane.add(btnVestido);
		lblBoquita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (inMano == "Chopstick_food") {
					inMano = "Chopstick";
					changeCursor("Food/Chopstick", 0, 0);
					altValue(hungerBar, 50);
				}
			}
		});

		label_6 = new JLabel("12:00");
		label_6.setOpaque(true);
		label_6.setBounds(0, 7, 80, 15);
		label_6.setHorizontalAlignment(4);
		contentPane.add(label_6);

		lblBoquita.setBounds(166, 180, 25, 15);
		contentPane.add(lblBoquita);
		contentPane.add(lbl_Cena);

		lblDucha.setBounds(128, 55, 90, 55);
		contentPane.add(lblDucha);

		btnHall.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				label.setIcon(new ImageIcon(SOURCEFOLDER + "Hallroom.png"));
				lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
				setFaceLocation(137, 125);
				inMano = "Pet";
				contentPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
				Pose = "De pie";
				lugaPlace = "Hall";
				hallroomObjects(true);
				kitchenObjects(false);
				bathroomObjects(false);
				bedroomObjects(false);
				ClosetObjects(false);
				ShopObjects(false);
			}
		});
		btnHall.setBounds(67, 506, 38, 40);
		contentPane.add(btnHall);

		JLabel btnBath = new JLabel("");
		btnBath.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				label.setIcon(new ImageIcon(SOURCEFOLDER + "Bathroom.png"));
				lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
				setFaceLocation(137, 125);
				inMano = "";
				contentPane.setCursor(Cursor.getDefaultCursor());
				Pose = "De pie";
				lugaPlace = "Bathroom";
				hallroomObjects(false);
				kitchenObjects(false);
				bathroomObjects(true);
				bedroomObjects(false);
				ClosetObjects(false);
				ShopObjects(false);
			}
		});
		btnBath.setBounds(180, 506, 41, 40);
		contentPane.add(btnBath);

		JLabel btnShop = new JLabel("");
		btnShop.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				label.setIcon(new ImageIcon(SOURCEFOLDER + "TiendaPotions.png"));
				inMano = "";
				contentPane.setCursor(Cursor.getDefaultCursor());
				Pose = "De pie";
				lugaPlace = "Shop";
				hallroomObjects(false);
				kitchenObjects(false);
				bathroomObjects(false);
				bedroomObjects(false);
				ClosetObjects(false);
				ShopObjects(true);
			}
		});
		btnShop.setBounds(25, 506, 41, 40);
		contentPane.add(btnShop);

		JLabel btnBedroom = new JLabel("");
		btnBedroom.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				label.setIcon(new ImageIcon(SOURCEFOLDER + "Bedroom.png"));
				lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
				setFaceLocation(137, 75);
				inMano = "";
				contentPane.setCursor(Cursor.getDefaultCursor());
				Pose = "Acostada";
				lugaPlace = "Bedroom";
				hallroomObjects(false);
				kitchenObjects(false);
				bathroomObjects(false);
				bedroomObjects(true);
				ClosetObjects(false);
				ShopObjects(false);
			}
		});
		btnBedroom.setBounds(237, 506, 41, 40);
		contentPane.add(btnBedroom);

		JLabel btnKitch = new JLabel("");
		btnKitch.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				label.setIcon(new ImageIcon(SOURCEFOLDER + "Kitchen.png"));
				lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
				setFaceLocation(137, 33);
				inMano = "Chopstick";
				changeCursor("Food/Chopstick", 0, 0);
				Pose = "De pie";
				lugaPlace = "Kitchen";
				hallroomObjects(false);
				kitchenObjects(true);
				bathroomObjects(false);
				bedroomObjects(false);
				ClosetObjects(false);
				ShopObjects(false);
			}
		});
		btnKitch.setBounds(122, 506, 41, 40);
		contentPane.add(btnKitch);

		btnEsponja.setIcon(new ImageIcon(SOURCEFOLDER + "Sponge.png"));
		btnEsponja.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (inMano == "") {
					inMano = "Esponja";
					btnEsponja.setIcon(null);
					changeCursor("Sponge", 27, 20);
				} else if (inMano == "Esponja") {
					contentPane.setCursor(Cursor.getDefaultCursor());
					inMano = "";
					btnEsponja.setIcon(new ImageIcon(SOURCEFOLDER + "Sponge.png"));
				}
			}

		});
		btnEsponja.setBounds(231, 180, 53, 38);
		contentPane.add(btnEsponja);

		lblCuerpo.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				if (inMano == "Esponja") {
					JLabel lblEspuma = new JLabel("");
					lblEspuma.setIcon(new ImageIcon(SOURCEFOLDER + "Espuma.png"));
					lblEspuma.setBounds(arg0.getX() - 22, arg0.getY() - 19, 43, 38);
					Component[] sa = contentPane.getComponents();
					altValue(cleanBar, 5);
					contentPane.removeAll();
					contentPane.add(lblEspuma);
					for (int i = 0; i < sa.length; i++)
						contentPane.add(sa[i]);
					contentPane.repaint();

					lblDucha.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							if (inMano == "")
								contentPane.remove(lblEspuma);
						}
					});

					MouseAdapter jassa = new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							contentPane.remove(lblEspuma);
						}
					};
					btnHall.addMouseListener(jassa);
					btnKitch.addMouseListener(jassa);
					btnBath.addMouseListener(jassa);
					btnBedroom.addMouseListener(jassa);

				}
			}
		});

		JLabel lblLowerMenu = new JLabel("");
		lblLowerMenu.setVerticalAlignment(SwingConstants.TOP);
		lblLowerMenu.setIcon(new ImageIcon(SOURCEFOLDER + "StatBar.png"));
		lblLowerMenu.setBounds(0, 500, 337, 52);
		contentPane.add(lblLowerMenu);
		lblMesa.setVerticalAlignment(SwingConstants.TOP);

		lblMesa.setIcon(new ImageIcon(SOURCEFOLDER + "Table.png"));
		lblMesa.setBounds(0, 272, 337, 230);
		contentPane.add(lblMesa);
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();

				if (lugaPlace.equals("Shop")) {
					if (valueInCords(x, y, 12, 40, 64, 125)) {
						if (money >= 50) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado azucar", "Compra realizada", 1);
							item_SugarBag++;
							money -= 50;
						}
					}
					if (valueInCords(x, y, 95, 40, 150, 125)) {
						if (money >= 50) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado harina", "Compra realizada", 1);
							item_FlourBag++;
							money -= 50;
						}
					}
					if (valueInCords(x, y, 180, 40, 240, 125)) {
						if (money >= 50) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado arroz", "Compra realizada", 1);
							item_RicesBag++;
							money -= 50;
						}
					}
					if (valueInCords(x, y, 270, 40, 320, 125)) {
						if (money >= 60) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado pasta", "Compra realizada", 1);
							item_PastaBag++;
							money -= 60;
						}
					}

					if (valueInCords(x, y, 25, 158, 55, 245)) {
						if (money >= 40) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado leche", "Compra realizada", 1);
							item_Milk++;
							money -= 40;
						}
					}
					if (valueInCords(x, y, 80, 158, 133, 245)) {
						if (money >= 40) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado huevos", "Compra realizada", 1);
							item_Eggs++;
							money -= 40;
						}
					}
					if (valueInCords(x, y, 155, 158, 220, 245)) {
						if (money >= 50) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado comida de gato",
									"Compra realizada", 1);
							item_CatFood++;
							money -= 50;
						}
					}
					if (valueInCords(x, y, 235, 158, 330, 245)) {
						if (money >= 5000) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado un kit médico", "Compra realizada",
									1);
							item_MedicKit++;
							money -= 5000;
						}
					}

					if (valueInCords(x, y, 20, 290, 80, 380)) {
						if (money >= 75) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado fruta", "Compra realizada", 1);
							item_Fruit++;
							money -= 75;
						}
					}
					if (valueInCords(x, y, 100, 290, 145, 380)) {
						if (money >= 75) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado salsa", "Compra realizada", 1);
							item_Tomatoe++;
							money -= 75;
						}
					}
					if (valueInCords(x, y, 165, 290, 215, 380)) {
						if (money >= 75) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado vegetales", "Compra realizada", 1);
							item_Veggies++;
							money -= 75;
						}
					}
					if (valueInCords(x, y, 235, 290, 315, 380)) {
						if (money >= 100) {
							JOptionPane.showMessageDialog(contentPane, "Has comprado carne", "Compra realizada", 1);
							item_Meat++;
							money -= 100;
						}
					}
					lblMoney.setText("" + money);
				}

			}
		});

		Hairstile.setBounds(0, 0, 337, 600);
		Hairstile.setVerticalAlignment(1);
		Hairstile.setIcon(new ImageIcon(SOURCEFOLDER + "Peinados/" + actualPeinado + ".png"));
		contentPane.add(Hairstile);

		DressDressed.setBounds(0, 0, 337, 502);
		DressDressed.setVerticalAlignment(1);
		DressDressed.setIcon(new ImageIcon(SOURCEFOLDER + "Dresses/" + actualVestido + ".png"));
		contentPane.add(DressDressed);

		lblCuerpo.setBounds(0, 0, 337, 600);
		lblCuerpo.setVerticalAlignment(1);
		lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
		contentPane.add(lblCuerpo);
		label.setVerticalAlignment(SwingConstants.TOP);

		label.setIcon(new ImageIcon(SOURCEFOLDER + "Index.png"));
		label.setBounds(0, 0, 337, 502);
		contentPane.add(label);

		btnSientate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Pose.equals("Sentada")) {
					lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
					Pose = "De pie";
					btnSientate.setText("Sientate");
					moveFaceLocation(0, -92);
				} else {
					lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "CuteSeat.png"));
					Pose = "Sentada";
					btnSientate.setText("Arriba");
					moveFaceLocation(0, 92);
					lblCuerpo.setBounds(lblCuerpo.getX(), lblCuerpo.getY() - 92, lblCuerpo.getWidth(),
							lblCuerpo.getHeight());

				}
			}
		});
		btnSientate.setBounds(350, 48, 100, 25);
		contentPane.add(btnSientate);

		btnAutoComer = new JButton("Auto Comer");
		btnAutoComer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Thread() {
					public void run() {
						AutoMoves.eat(5);
					}
				}.start();
			}
		});
		btnAutoComer.setBounds(350, 85, 100, 25);
		contentPane.add(btnAutoComer);

		tglAutonomy = new JToggleButton("Autonomia");
		tglAutonomy.setBounds(355, 137, 95, 25);
		contentPane.add(tglAutonomy);

		setUndecorated(true);

	}



	void confgburing(JProgressBar boraboraBar, int x, int y, int horaduracion) {

		boraboraBar.setForeground(new Color(173, 255, 47));
		boraboraBar.setMaximum(horaduracion * 60);
		boraboraBar.setMinimum(0);
		boraboraBar.setValue(horaduracion * 60);
		boraboraBar.setBounds(x, y, 10, 40);
		boraboraBar.setVisible(false);

		btnLamp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnLamp.isSelected()) {
					emoimageset(EmotionFaceEye, "Eyes/Closed");
					Pose = "Dormida";
				} else {
					emoimageset(EmotionFaceEye, "Eyes/Cute");
					Pose = "Acostada";
				}
			}
		});

		JLabel lblCloset = new JLabel("Closet");
		lblCloset.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				label.setIcon(new ImageIcon(SOURCEFOLDER + "Closet.png"));
				lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
				setFaceLocation(137, 75);
				inMano = "Pet";
				contentPane.setCursor(Cursor.getDefaultCursor());
				Pose = "De Pie";
				lugaPlace = "Closet";
				hallroomObjects(false);
				kitchenObjects(false);
				bathroomObjects(false);
				bedroomObjects(false);
				ClosetObjects(true);
				ShopObjects(false);
			}
		});

		JButton btnNewButton = new JButton("X");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnNewButton.setBounds(298, 7, 35, 35);
		contentPane.add(btnNewButton);
		JLabel label_1 = new JLabel("");
		label_1.setOpaque(true);
		boolean misasa = true;
		btnVestido.setVisible(false);
		JLabel label_1_1 = new JLabel("");
		label_1_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (misasa) {
					label_1.setVisible(false);
					label_1_1.setVisible(false);
					button_1.setVisible(true);
					button_2.setVisible(true);
					HalllamouseClicked();
					timerendo();
					for (int i = 0; i < 5; i++)
						AutoMoves.mouseClick();
				}
			}
		});
		label_1_1.setBounds(247, 309, 28, 30);
		contentPane.add(label_1_1);

		// label_1.addMouseListener(new MouseAdapter() {
		// @Override
		// public void mouseClicked(MouseEvent e) {
		// }
		// });
		label_1.setIcon(new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título 2/Hab/aassa/Index.png"));
		label_1.setVerticalAlignment(SwingConstants.TOP);
		label_1.setBounds(0, 2, 337, 550);
		contentPane.add(label_1);

		lblMoney.setOpaque(true);
		lblMoney.setBounds(120, 8, 66, 20);
		contentPane.add(lblMoney);
		lblCloset.setBounds(294, 511, 39, 35);
		contentPane.add(lblCloset);
		btnLamp.setBounds(259, 259, 74, 25);
		contentPane.add(btnLamp);
		boraboraBar.setOrientation(SwingConstants.VERTICAL);
		contentPane.add(boraboraBar);
		
	}

	void hallroomObjects(boolean visusable) {
	}

	void kitchenObjects(boolean visusable) {
		lbl_Cena.setVisible(visusable);
		lblMesa.setVisible(visusable);
	}

	void bathroomObjects(boolean visusable) {
		btnEsponja.setVisible(visusable);
		lblDucha.setVisible(visusable);
		DressDressed.setVisible(!visusable);
	}

	void bedroomObjects(boolean visusable) {
		btnSientate.setVisible(!visusable);
		btnLamp.setVisible(visusable);
		btnAutoComer.setVisible(!visusable);
	}

	void ClosetObjects(boolean visusable) {
		btnPeinado.setVisible(visusable);
		btnVestido.setVisible(visusable);
	}

	void ShopObjects(boolean visusable) {
		EmotionFaceEyeBrown.setVisible(!visusable);
		EmotionFaceEye.setVisible(!visusable);
		EmotionFaceMouth.setVisible(!visusable);
		EmotionFaceSleepy.setVisible(!visusable);
		EmotionFaceBlush.setVisible(!visusable);
		lblBoquita.setVisible(!visusable);
		lblCuerpo.setVisible(!visusable);
		Hairstile.setVisible(!visusable);
		DressDressed.setVisible(!visusable);

		hungerLabel.setVisible(!visusable);
		sleepLabel.setVisible(!visusable);
		cleanLabel.setVisible(!visusable);
		funLabel.setVisible(!visusable);
		label_IMC.setVisible(!visusable);
	}

	void addRoomChanger(String varPlace) {
		btnHall.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				label.setIcon(new ImageIcon(SOURCEFOLDER + varPlace + ".png"));
				lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
				setFaceLocation(137, 125);
				inMano = "Pet";
				Pose = "De pie";
				hallroomObjects(true);
				kitchenObjects(false);
				bathroomObjects(false);
				bedroomObjects(false);
			}
		});
	}

	JLabel lblMoney = new JLabel("1000");
	int money = 1000;
	int item_SugarBag = 0;
	int item_FlourBag = 0;
	int item_RicesBag = 0;
	int item_PastaBag = 0;
	int item_Milk = 0;
	int item_Eggs = 0;
	int item_CatFood = 0;
	int item_MedicKit = 0;
	int item_Fruit = 0;
	int item_Tomatoe = 0;
	int item_Veggies = 0;
	int item_Meat = 0;

	boolean valueInCords(int mx, int my, int x, int y, int x2, int y2) {
		return (mx >= x & mx <= x2) & (my >= y & my <= y2);
	}

	void petGraphicReaction(String imgCursor, String eyebrown, String eyes, String mouth, double intmod, int funmod) {
		changeCursor("Pet/" + imgCursor, 12, 12);
		emoimageset(EmotionFaceEyeBrown, "Eyebrown/" + eyebrown);
		emoimageset(EmotionFaceEye, "Eyes/" + eyes);
		emoimageset(EmotionFaceMouth, "Mouth/" + mouth);
		intimityProgress += intmod;
		altValue(funBar, funmod);
	}

	void changeCursor(String dirCursor, int w, int h) {
		contentPane.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
				Toolkit.getDefaultToolkit().getImage(SOURCEFOLDER + "Cursor/" + dirCursor + ".png"), new Point(w, h),
				"img"));
	}

	static int veltime = 100;
	int hora = 6;
	int dia = 1;
	int minu = 0;
	private JToggleButton tglAutonomy;
	private JButton btnAutoComer;
	private JButton button_1;
	private JButton button_2;

	void timerendo() {

		new Thread() {
			public void run() {
				while (true) {
					for (byte b = 0; b < 30; b++) {
						try {
							Thread.sleep(veltime);
						} catch (InterruptedException e) {
						}
						minu++;
						if (minu == 60) {
							minu = 0;
							hora++;
							if (hora == 24) {
								hora = 0;
								dia++;
							} else if (tglAutonomy.isSelected()) {
								if (hora == 22) {
									new Thread() {
										public void run() {
											AutoMoves.sleep();
										}
									}.start();
								} else if (hora == 6) {
									new Thread() {
										public void run() {
											AutoMoves.awake();
										}
									}.start();
								} else if (hora == 7 | hora == 12 | hora == 18) {
									new Thread() {
										public void run() {
											AutoMoves.eat(7);
										}
									}.start();
								} else if (hora == 20) {
									new Thread() {
										public void run() {
											AutoMoves.shower();
										}
									}.start();
								}
							}

						}
						if (minu < 10)
							label_6.setText(hora + ":0" + minu + " Dia " + dia);
						else
							label_6.setText(hora + ":" + minu + " Dia " + dia);
						if (!Pose.equals("Dormida")) {
							altValue(hungerBar, -1);
							if (hungerBar.getValue() == 0)
								IMCValue -= 0.0005;
							else
								IMCValue += 0.0005;
						}
						updateBars();
					}

					if (Pose.equals("Dormida")) {
						altValue(sleepBar, +90);
						altValue(cleanBar, -5);
						altValue(hungerBar, -5);
						if (hungerBar.getValue() == 0)
							IMCValue -= 0.0025;
						else
							IMCValue += 0.0025;
					} else {
						altValue(sleepBar, -30);
						altValue(cleanBar, -30);
						// altValue(hungerBar, -50);
						altValue(funBar, -40);
					}

					// UpdatoAllBars();
					if (IMCValue <= 16)
						System.exit(0);
				}
			}
		}.start();
	}

	static void altValue(JProgressBar asaBar, int valor) {
		asaBar.setValue(asaBar.getValue() + valor);
	}

	void updateBars() {
		label_IMC.setText("IMC=" + ((int) (IMCValue * 100)) / 100.00);

		if (hungerBar.getValue() == 0) {
			hungerLabel.setText("Estomago Vacío");
			altValue(funBar, -1);
		} else if (hungerBar.getValue() <= 250)
			hungerLabel.setText("Satisfecha");
		else if (hungerBar.getValue() <= 500)
			hungerLabel.setText("Llena");
		else if (hungerBar.getValue() <= 750) {
			hungerLabel.setText("Rellena");
			altValue(funBar, -1);
		} else {
			hungerLabel.setText("A reventar");
			altValue(funBar, -2);
		}

		if (sleepBar.getValue() <= 2 * 60) {
			sleepLabel.setText("Agotada");
			altValue(funBar, -3);
		} else if (sleepBar.getValue() <= 4 * 60) {
			sleepLabel.setText("Muy cansada");
			altValue(funBar, -2);
		} else if (sleepBar.getValue() <= 8 * 60) {
			sleepLabel.setText("Cansada");
			altValue(funBar, -1);
			emoimageset(EmotionFaceSleepy, "Others/Sleepy");
		} else if (sleepBar.getValue() <= 16 * 60) {
			sleepLabel.setText("Algo cansada");
			emoimageset(EmotionFaceSleepy, "Others/null");
		} else {
			sleepLabel.setText("Despierta");
			altValue(funBar, +1);
		}

		if (cleanBar.getValue() <= 2 * 60) {
			cleanLabel.setText("Apesta");
			altValue(funBar, -2);
		} else if (cleanBar.getValue() <= 8 * 60) {
			cleanLabel.setText("Huele muy mal");
			altValue(funBar, -1);
		} else if (cleanBar.getValue() <= 16 * 60)
			cleanLabel.setText("Huele algo mal");
		else if (cleanBar.getValue() <= 26 * 60) {
			cleanLabel.setText("No huele mal");
		} else {
			cleanLabel.setText("Huele bien");
			altValue(funBar, +1);
		}

		if (funBar.getValue() <= 13 * 60)
			funLabel.setText("Está deprimida");
		else if (funBar.getValue() <= 26 * 60)
			funLabel.setText("Está muy triste");
		else if (funBar.getValue() <= 39 * 60)
			funLabel.setText("Está triste");
		else if (funBar.getValue() <= 52 * 60)
			funLabel.setText("No está felíz");
		else
			funLabel.setText("Está felíz");

	}

	void updateIntimity() {
		if (intimityProgress > 255)
			intimityProgress = 255;

	}

	void ConfImageLabel(JLabel configurimage, int x, int y, int w, int h, String dir) {
		configurimage.setBounds(x, y, w, h);
		configurimage.setIcon(new ImageIcon(SOURCEFOLDER + dir + ".png"));
		configurimage.setHorizontalAlignment(2);
		configurimage.setVerticalAlignment(1);
		contentPane.add(configurimage);
	}

	void ConfFaceLabel(JLabel configurimage, String dir) {
		configurimage.setBounds(137, 125, 80, 80);
		emoimageset(configurimage, dir);
		configurimage.setHorizontalAlignment(2);
		configurimage.setVerticalAlignment(1);
		contentPane.add(configurimage);
	}

	void buttonAltVeltime(JButton buttonvar, int veloci) {
		buttonvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				veltime = veloci;
			}
		});
	}

	void emoimageset(JLabel emoimage, String dir) {
		emoimage.setIcon(new ImageIcon(SOURCEFOLDER + "Emotions/" + dir + ".png"));
	}

	void moveFaceLocation(int x, int y) {
		setFaceLocation(EmotionFaceEye.getX() + x, EmotionFaceEye.getY() + y);
	}

	public void HalllamouseClicked() {
		label.setIcon(new ImageIcon(SOURCEFOLDER + "Hallroom.png"));
		lblCuerpo.setIcon(new ImageIcon(SOURCEFOLDER + "Cute.png"));
		setFaceLocation(137, 125);
		inMano = "Pet";
		contentPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
		Pose = "De pie";
		lugaPlace = "Hall";
		hallroomObjects(true);
		kitchenObjects(false);
		bathroomObjects(false);
		bedroomObjects(false);
		ClosetObjects(false);
		ShopObjects(false);
	}

	void setFaceLocation(int x, int y) {
		EmotionFaceEyeBrown.setBounds(x, y, 80, 80);
		EmotionFaceEye.setBounds(x, y, 80, 80);
		EmotionFaceMouth.setBounds(x, y, 80, 80);
		EmotionFaceSleepy.setBounds(x, y, 80, 80);
		EmotionFaceBlush.setBounds(x, y, 80, 80);
		lblBoquita.setBounds(x + 29, y + 55, 25, 15);
		lblCuerpo.setBounds(x - 137, y - 125, lblCuerpo.getWidth(), lblCuerpo.getHeight());
		Hairstile.setBounds(x - 137, y - 125, DressDressed.getWidth(), DressDressed.getHeight());
		DressDressed.setBounds(x - 137, y - 125, DressDressed.getWidth(), DressDressed.getHeight());
	}
}
