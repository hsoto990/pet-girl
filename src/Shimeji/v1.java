package Shimeji;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class v1 extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					v1 frame = new v1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	int screenWidth = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	int screenHeight = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();

	public v1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, screenWidth, screenHeight);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setUndecorated(true);

		CreateDragableObject("Aysel", 10, 30);
		CreateDragableObject("Sponge", 30, 20);
		CreateDragableObject("Food", 150, 20);
	}


	static int draqgedObj = 0;
	static int lastObj = 0;

	void CreateDragableObject(String varName, int x, int y) {
		lastObj++;
		int id = lastObj;
		JLabel varDrag = new JLabel("" + id);
		varDrag.setName(varName);
		varDrag.setIcon(new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título 2/" + varName + "_Stand.png"));
		varDrag.setBounds(x, y, varDrag.getIcon().getIconWidth(), varDrag.getIcon().getIconHeight());
		varDrag.setHorizontalAlignment(SwingConstants.LEFT);
		varDrag.setVerticalAlignment(SwingConstants.TOP);
		contentPane.add(varDrag);

		varDrag.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent arg0) {

				if (draqgedObj == id) {
					draqgedObj = 0;
					varDrag.setIcon(
							new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título 2/" + varName + "_Stand.png"));
					varDrag.setBounds(varDrag.getX()+1000-varDrag.getIcon().getIconWidth()/2, varDrag.getY()+1000-varDrag.getIcon().getIconHeight()/2, varDrag.getIcon().getIconWidth(),
							varDrag.getIcon().getIconHeight());
					varDrag.setHorizontalAlignment(SwingConstants.LEFT);
					varDrag.setVerticalAlignment(SwingConstants.TOP);
				} else if (draqgedObj == 0) {
					draqgedObj = id;
					
					varDrag.setIcon(
							new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título 2/" + varName + "_Carry.png"));
					varDrag.setBounds(varDrag.getX()-1000, varDrag.getY()-1000, 2000, 2000);
					varDrag.setHorizontalAlignment(SwingConstants.CENTER);
					varDrag.setVerticalAlignment(SwingConstants.CENTER);
					varDrag.setLocation(new Point(arg0.getXOnScreen()  -1000,
							arg0.getYOnScreen() -1000));
					int lasuObj=lastObj;
					for (int i = 0; i < lasuObj; i++) {
						JLabel mikuru = (JLabel) contentPane.getComponent(i);
						if (!mikuru.getText().equals("" + id)) {
							contentPane.remove(i);
							contentPane.add(mikuru);
							i--;
							lasuObj--;
						}
					}
				}
			}
		});
		varDrag.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				if (draqgedObj == id) {
					varDrag.setLocation(new Point(arg0.getXOnScreen()  -1000,
							arg0.getYOnScreen() -1000));
				}
			}
		});
		v1.this.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				if (draqgedObj == id) {
					varDrag.setLocation(new Point(arg0.getXOnScreen()  -1000,
							arg0.getYOnScreen() -1000));
				}
			}
		});
		new Thread() {
			public void run() {
				while (true) {
					if (draqgedObj != id) {
						if (varDrag.getY() >= screenHeight - varDrag.getHeight())
							varDrag.setLocation(new Point(varDrag.getX(), screenHeight - varDrag.getHeight()));
						else
							varDrag.setLocation(new Point(varDrag.getX(), varDrag.getY() + 1));
					}
					try {
						Thread.sleep(10);
					} catch (Exception e) {
					}
				}

			}
		}.start();
	}
}
