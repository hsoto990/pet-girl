import java.awt.AWTException;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import java.awt.Cursor;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Java 360 Sphere Image Viewer.
 * 
 * Resources: factory image:
 * https://i.ytimg.com/vi/Ifis9sSyPSY/maxresdefault.jpg park image:
 * http://www.mediavr.com/belmorepark1left.jpg
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
public class Viewanita extends Canvas implements MouseMotionListener, MouseListener {
	private static final long serialVersionUID = 1L;

	private static long dias = 1;
	private static byte hora = 12;
	private static byte minu = 0;

	private BufferedImage sphereImage;
	private final BufferedImage offscreenImage;
	private int[] sphereImageBuffer;
	private final int[] offscreenImageBuffer;
	private static final double FOV = 2;
	private final double cameraPlaneDistance;
	private double rayVecs[][][];
	private static final double ACCURACY_FACTOR = 500;
	private static final int REQUIRED_SIZE = (int) (2 * ACCURACY_FACTOR);
	private double[] asinTable;
	private double[] atan2Table;
	private static final double INV_PI = 1 / Math.PI;
	private static final double INV_2PI = 1 / (2 * Math.PI);
	private static double currentRotationY;
	static String placezar = "Hall";
	static String timezar = "Day";
	static int SkyColorR = 255;
	static int SkyColorG = 255;
	static int SkyColorB = 255;

	private static double currentRotationX;
	private int mouseX, mouseY;
	private BufferStrategy bs;
	static BufferedImage ImageMouse;
	boolean saa = true;

	static JLabel lblDia = new JLabel("Día 1");
	static JLabel lblHora = new JLabel("7:00");
	static JLabel finalFace = new JLabel();
	static JLabel lblNotOkay = new JLabel("Aysel se ha ido");
	static JLabel label_17 = new JLabel();

	static JLabel lblNewBase = new JLabel("");
	static JLabel lblNewSkin = new JLabel("");
	static JLabel lblNewHair = new JLabel("");
	static int peinado = 1;
	static int cantidpeinado = 9;

	static JProgressBar BaraWater = new JProgressBar();
	static JProgressBar BaraStomach = new JProgressBar();
	static JProgressBar BaraPipi = new JProgressBar();
	static JProgressBar BaraBoring = new JProgressBar();
	static JProgressBar BaraSleep = new JProgressBar();
	static JProgressBar BaraSweet = new JProgressBar();
	static JProgressBar BaraStress = new JProgressBar();
	static JProgressBar BaraClean = new JProgressBar();
	static JButton btnSiguiente = new JButton(">");
	static JButton btnAnterior = new JButton("<");

	public Viewanita() {
		ConfigureProg(BaraWater, 57, 148, 10, 40, 2000, 1000, Color.CYAN, new Color(0, 100, 123));
		ConfigureProg(BaraStomach, 74, 148, 20, 40, 2000, 1000, new Color(255, 133, 0), new Color(110, 65, 0));
		ConfigureProg(BaraPipi, 70, 195, 10, 40, 2000, 1000, Color.ORANGE, new Color(141, 64, 25));

		ConfigureProg(BaraBoring, 10, 20, 10, 40, 2000, 1000, Color.GREEN, new Color(25, 141, 64));
		ConfigureProg(BaraSleep, 26, 20, 10, 40, 2000, 1000, Color.BLUE, new Color(25, 64, 141));

		ConfigureProg(BaraSweet, 113, 20, 10, 40, 2000, 1000, Color.PINK, new Color(141, 25, 64));
		ConfigureProg(BaraStress, 130, 20, 10, 40, 2000, 1000, Color.RED, new Color(141, 64, 25));

		ConfigureProg(BaraClean, 15, 100, 10, 40, 2000, 1000, Color.CYAN, new Color(0, 100, 123));
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (peinado == 0)
					peinado = cantidpeinado;
				else
					peinado--;
				lblNewHair.setIcon(
						new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título/Peinados/" + peinado + ".png"));
			}
		});
		frame.getContentPane().add(btnAnterior);
		btnAnterior.setBounds(300, 240, 70, 20);
		btnAnterior.setVisible(false);
		
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (peinado == cantidpeinado)
					peinado = 0;
				else
					peinado++;
				lblNewHair.setIcon(
						new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título/Peinados/" + peinado + ".png"));
			}
		});
		frame.getContentPane().add(btnSiguiente);
		btnSiguiente.setBounds(700, 240, 70, 20);
		btnSiguiente.setVisible(false);

		lblDia.setFont(new Font("Dialog", Font.BOLD, 18));
		lblDia.setForeground(Color.WHITE);
		lblDia.setBounds(10, 360, 86, 86);
		frame.getContentPane().add(lblDia);

		lblHora.setFont(new Font("Dialog", Font.BOLD, 18));
		lblHora.setForeground(Color.WHITE);
		lblHora.setBounds(10, 380, 86, 86);
		frame.getContentPane().add(lblHora);

		lblNewHair.setIcon(new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título/Peinados/0.png"));
		lblNewHair.setBounds(0, 0, 150, 500);
		frame.getContentPane().add(lblNewHair);
		lblNewSkin.setIcon(new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título/BodyCape.png"));
		lblNewSkin.setBounds(0, 0, 150, 500);
		frame.getContentPane().add(lblNewSkin);
		lblNewBase.setIcon(new ImageIcon("/home/ceibal/Escritorio/Carpeta sin título/BackCape.png"));
		lblNewBase.setBounds(0, 0, 150, 500);
		frame.getContentPane().add(lblNewBase);
		frame.setUndecorated(true);
		finalFace.setIcon(new ImageIcon("/home/ceibal/Escritorio/facesa/Face/Final.png"));
		finalFace.setBounds(106 + 70 + 400 - 86, 100, 171, 172);
		frame.getContentPane().add(finalFace);
		lblNotOkay.setHorizontalAlignment(SwingConstants.CENTER);
		lblNotOkay.setBounds(106 + 70 + 400 - 250, 260, 500, 86);
		frame.getContentPane().add(lblNotOkay);
		finalFace.setVisible(false);
		lblNotOkay.setVisible(false);

		label_17.setBounds(106, 0, 800, 500);
		frame.getContentPane().add(label_17);
		label_17.setVisible(false);

		chargeImage();
		offscreenImage = new BufferedImage(800, 500, BufferedImage.TYPE_INT_RGB);
		offscreenImageBuffer = ((DataBufferInt) offscreenImage.getRaster().getDataBuffer()).getData();
		cameraPlaneDistance = (offscreenImage.getWidth() / 2) / Math.tan(FOV / 2);
		createRayVecs();
		precalculateAsinAtan2();
		addMouseMotionListener(this);
		addMouseListener(this);
		RobotMouseMove(frame.getX() + 150 + 400, frame.getY() + 250);

	}

	static void ConfigureProg(JProgressBar progressBara, int x, int y, int w, int h, int M, int v, Color trick,
			Color treat) {
		progressBara.setBounds(x, y, w, h);
		progressBara.setMaximum(M);
		progressBara.setMinimum(0);
		progressBara.setValue(v);
		progressBara.setOrientation(SwingConstants.VERTICAL);
		frame.getContentPane().add(progressBara);
		progressBara.setForeground(trick);
		progressBara.setBackground(treat);
	}

	static int veltime = 500;
	static Boolean isOkay = true;

	void timerendo() {
		new Thread() {
			public void run() {
				while (isOkay) {

					for (byte b = 0; b < 30; b++) {

						try {
							Thread.sleep(veltime);
						} catch (InterruptedException e) {
						}
						minu++;
						if (minu == 60) {
							minu = 0;
							hora++;
							if (hora == 24) {
								hora = 0;
								dias++;
								lblDia.setText("Día " + dias);
							}
						}
						if (minu < 10)
							lblHora.setText(hora + ":0" + minu);
						else
							lblHora.setText(hora + ":" + minu);
					}
					if (BaraWater.getValue() >= 30) {
						BaraWater.setValue(BaraWater.getValue() - 30);
						BaraPipi.setValue(BaraPipi.getValue() + 25);
					} else {
						BaraPipi.setValue(BaraPipi.getValue() + BaraWater.getValue());
						BaraWater.setValue(0);
					}
					if (BaraStomach.getValue() >= 30) {
						BaraStomach.setValue(BaraStomach.getValue() - 30);
					} else {
						BaraStomach.setValue(0);
					}
					BaraClean.setValue(BaraClean.getValue() - 25);
					BaraSleep.setValue(BaraSleep.getValue() - 25);
					BaraSweet.setValue(BaraSweet.getValue() - 25);

					if (hora >= 1 & hora <= 6) {
						SkyColorR += 15;
						SkyColorG += 11;
					} else if (hora == 7) {
						SkyColorR += 15;
						SkyColorG += 11;
						SkyColorB += 16;
					} else if (hora == 8) {
						SkyColorG += 11;
						SkyColorB += 16;
					} else if (hora == 9 | hora == 10) {
						SkyColorG += 11;
						SkyColorB += 16;
					} else if (hora == 11) {
						SkyColorB += 16;
					} else if (hora == 13) {
						SkyColorB -= 16;
					} else if (hora == 14) {
						SkyColorG -= 11;
						SkyColorB -= 16;
					} else if (hora == 15) {
						SkyColorG -= 11;
						SkyColorB -= 16;
					} else if (hora == 16) {
						SkyColorG -= 11;
						SkyColorB -= 16;
					} else if (hora == 17) {
						SkyColorR -= 15;
						SkyColorG -= 11;
						SkyColorB -= 16;
					} else if (hora >= 18) {
						SkyColorR -= 15;
						SkyColorG -= 11;
					}
				}
			}
		}.start();
	}

	static void altValue(int asaBar, int valor) {
		asaBar -= valor;
	}

	void chargeImage() {
		currentRotationY = 0.0;
		try {
			BufferedImage sphereTmpImage = ImageIO.read(getClass().getResourceAsStream("/res/" + placezar + ".png"));
			sphereImage = new BufferedImage(sphereTmpImage.getWidth(), sphereTmpImage.getHeight(),
					BufferedImage.TYPE_INT_RGB);
			sphereImage.getGraphics().drawImage(sphereTmpImage, 0, 0, null);
			sphereImageBuffer = ((DataBufferInt) sphereImage.getRaster().getDataBuffer()).getData();
			RobotMouseMove(Viewanita.frame.getX() + 400, Viewanita.frame.getY() + 249);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, "Panorama \"" + placezar + "\" no encontrado.", "Advertencia", 0);
			System.exit(-1);
		}
	}

	private void createRayVecs() {
		rayVecs = new double[offscreenImage.getWidth()][offscreenImage.getHeight()][3]; // x, y, z
		for (int y = 0; y < offscreenImage.getHeight(); y++) {
			for (int x = 0; x < offscreenImage.getWidth(); x++) {
				double vecX = x - offscreenImage.getWidth() / 2;
				double vecY = y - offscreenImage.getHeight() / 2;
				double vecZ = cameraPlaneDistance;
				double invVecLength = 1 / Math.sqrt(vecX * vecX + vecY * vecY + vecZ * vecZ);
				rayVecs[x][y][0] = vecX * invVecLength;
				rayVecs[x][y][1] = vecY * invVecLength;
				rayVecs[x][y][2] = vecZ * invVecLength;
			}
		}
	}

	private void precalculateAsinAtan2() {
		asinTable = new double[REQUIRED_SIZE];
		atan2Table = new double[REQUIRED_SIZE * REQUIRED_SIZE];
		for (int i = 0; i < 2 * ACCURACY_FACTOR; i++) {
			asinTable[i] = Math.asin((i - ACCURACY_FACTOR) * 1 / ACCURACY_FACTOR);
			for (int j = 0; j < 2 * ACCURACY_FACTOR; j++) {
				double y = (i - ACCURACY_FACTOR) / ACCURACY_FACTOR;
				double x = (j - ACCURACY_FACTOR) / ACCURACY_FACTOR;
				atan2Table[i + j * REQUIRED_SIZE] = Math.atan2(y, x);
			}
		}
	}

	Graphics2D g;

	public void start() {
		createBufferStrategy(2);
		bs = getBufferStrategy();
		g = (Graphics2D) bs.getDrawGraphics();
		draw(g);
		bs.show();
		timerendo();
		RobotMouseMove(frame.getX() + 505, frame.getY() + 499);
	}

	static void RobotMouseMove(int posX, int posY) {
		try {
			Robot r = new Robot();
			r.mouseMove(posX, posY);
		} catch (AWTException e1) {
		}
	}

	static void addProgressBar(JProgressBar Barradera, int altura, int valor) {
		Barradera.setForeground(new Color(173, 255, 47));
		Barradera.setMaximum(1000);
		Barradera.setValue(valor);
		Barradera.setBounds(10, altura, 86, 15);
		frame.getContentPane().add(Barradera);
	}

	void adistener(String mximage, int sleepVar, int sweetVar, int sanityVar, int socialVar, int funVar, int toiletVar,
			int cleanVar, int hungerVar, int thirstVar, int izamax, int timadado) {
		try {
			label_17.setIcon(new ImageIcon("/home/ceibal/Escritorio/asdsa/Acciones/Kitchen/" + mximage + ".png"));
			saa = false;
			label_17.setVisible(true);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(frame, "Imagen \"" + mximage + "\" no encontrado.", "Advertencia", 0);
		}
		new Thread() {
			public void run() {
				try {
					veltime = timadado;
					for (int i = 0; i < izamax; i++) {
						Thread.sleep((long) (veltime));
						BaraWater.setValue(BaraWater.getValue() + thirstVar);
						BaraStomach.setValue(BaraStomach.getValue() + hungerVar);
						BaraPipi.setValue(BaraPipi.getValue() + toiletVar);
						BaraBoring.setValue(BaraBoring.getValue() + funVar);
						BaraSleep.setValue(BaraSleep.getValue() + sleepVar);
						BaraSweet.setValue(BaraSweet.getValue() + sweetVar);
						BaraStress.setValue(BaraStress.getValue() + sanityVar);
						BaraClean.setValue(BaraClean.getValue() + cleanVar);
					}
				} catch (InterruptedException e) {
					System.out.println(e);
				}
				label_17.setVisible(false);
				saa = true;
				RobotMouseMove(frame.getX() + 505, frame.getY() + 251);
				veltime = 500;
			}
		}.start();

	}

	private void draw(Graphics2D g) {
		if (isOkay & saa) {
			RobotMouseMove(Viewanita.frame.getX() + 400 + 150, Viewanita.frame.getY() + 250);
			double targetRotationX = (mouseY - 250) * 0.025;
			double targetRotationY = (mouseX - (400 + 150)) * 0.025;
			currentRotationY += (targetRotationX / 2);
			currentRotationX -= (targetRotationY / 2);
			if (currentRotationX > 6.25)
				currentRotationX -= 6.25;
			else if (currentRotationX < 0)
				currentRotationX += 6.25;
			if (currentRotationY > 1.52)
				currentRotationY = 1.52;
			else if (currentRotationY < -1.63)
				currentRotationY = -1.63;

			// System.out.println(LeoToGridY(currentRotationY) + "Y " +
			// LeoToGridX(currentRotationX) + "X");
			double sinRotationX = Math.sin(currentRotationY);
			double cosRotationX = Math.cos(currentRotationY);
			double sinRotationY = Math.sin(currentRotationX);
			double cosRotationY = Math.cos(currentRotationX);
			double tmpVecX, tmpVecY, tmpVecZ;
			for (int y = 0; y < offscreenImage.getHeight(); y++) {
				for (int x = 0; x < offscreenImage.getWidth(); x++) {
					double vecX = rayVecs[x][y][0];
					double vecY = rayVecs[x][y][1];
					double vecZ = rayVecs[x][y][2];
					// rotate x
					tmpVecZ = vecZ * cosRotationX - vecY * sinRotationX;
					tmpVecY = vecZ * sinRotationX + vecY * cosRotationX;
					vecZ = tmpVecZ;
					vecY = tmpVecY;
					// rotate y
					tmpVecZ = vecZ * cosRotationY - vecX * sinRotationY;
					tmpVecX = vecZ * sinRotationY + vecX * cosRotationY;
					vecZ = tmpVecZ;
					vecX = tmpVecX;
					int iX = (int) ((vecX + 1) * ACCURACY_FACTOR);
					int iY = (int) ((vecY + 1) * ACCURACY_FACTOR);
					int iZ = (int) ((vecZ + 1) * ACCURACY_FACTOR);
					// https://en.wikipedia.org/wiki/UV_mapping
					double u = 0.5 + (atan2Table[iZ + iX * REQUIRED_SIZE] * INV_2PI);
					double v = 0.5 - (asinTable[iY] * INV_PI);
					int tx = (int) (sphereImage.getWidth() * u);
					int ty = (int) (sphereImage.getHeight() * (1 - v));
					Color safa = new Color(sphereImageBuffer[ty * sphereImage.getWidth() + tx]);
					safa = new Color(safa.getRed() * SkyColorR / 255, safa.getGreen() * SkyColorG / 255,
							safa.getBlue() * SkyColorB / 255);
					offscreenImageBuffer[y * offscreenImage.getWidth() + x] = safa.getRGB();

				}
			}
			g.drawImage(offscreenImage, 800 + 150, 0, -800, 500, null);
			g.setColor(Color.BLACK);
			g.drawRect(395 + 106 + 70, 249, 10, 2);
			g.drawRect(399 + 106 + 70, 245, 2, 10);
			g.setColor(Color.WHITE);
			g.drawLine(396 + 106 + 70, 250, 404 + 106 + 70, 250);
			g.drawLine(400 + 106 + 70, 246, 400 + 106 + 70, 254);
			bs.show();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// do nothing
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		draw(g);
	}

	static JFrame frame = new JFrame();

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Viewanita viewa = new Viewanita();
				frame.setTitle("Vieware");
				frame.setBounds(100, 50, 950, 500);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
						new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "blank cursor"));
				frame.setLocationRelativeTo(null);
				frame.getContentPane().add(viewa);
				frame.setResizable(false);
				frame.setVisible(true);
				viewa.start();
				viewa.requestFocus();
			}
		});
	}

	static double LeoToGridX(double varX) {
		double dadasa = ((varX + 1.63) * 200 - 1270);
		if (dadasa < 0)
			dadasa = -dadasa;
		else
			dadasa = 1247 - dadasa;
		return dadasa / 63;
	}

	static double LeoToGridY(double varY) {
		return ((varY + 1.63) * 100) / 32;
	}

	static boolean CompGridEvent(double varGX1, double varGY1, double varGX2, double varGY2) {
		if (LeoToGridX(currentRotationX) >= varGX1 & LeoToGridY(currentRotationY) >= varGY1
				& LeoToGridX(currentRotationX) <= varGX2 & LeoToGridY(currentRotationY) <= varGY2)
			return true;
		else
			return false;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

		switch (placezar) {
		case "Kitchen":
			acciina2(1.5, 5.80, 2.2, 6.5, "Juguera", 0, 0, 5, 0, 0, 0, 0, 0, 0, 5);
			moviina(3.75, 4, 5, 6.8, "Hall");
			acciina2(5.5, 5.2, 6.75, 7.2, "Heladera", 0, 0, 5, 0, 0, 0, 0, 0, 0, 5);
			if (hora >= 4 & hora < 12)
				acciina2(9, 6, 11, 7, "Bacon and eggs", 0, 0, 0, 0, 0, 0, 0, 40, 0, 20);
			else
				acciina2(9, 6, 11, 7, "Carbonara", 0, 0, 0, 0, 0, 0, 0, 40, 0, 20);
			acciina2(11.3, 4.8, 12.2, 5.75, "Regar maceta", 0, 0, 5, 2, 1, 0, 0, 0, 0, 10);
			acciina2(15.2, 5.6, 16.5, 6.2, "Hornallas", 0, 0, 5, 0, 0, 0, 0, 0, 0, 5);
			acciina2(16.7, 5.4, 17.6, 6.1, "Hornito", 0, 0, 5, 0, 0, 0, 0, 0, 0, 5);
			acciina2(18.3, 5.6, 19.4, 6.2, "Beber", 0, 0, 0, 0, 0, 0, 0, 0, 120, 5);
			break;
		case "Hall":
			acciina2(19.2, 4, 20, 6, "Estantes", 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);
			acciina2(0, 4, 2.5, 6, "Estantes", 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);
			acciina2(2.6, 4.3, 3.6, 5.3, "Regar maceta", 0, 0, 5, 2, 1, 0, 0, 0, 0, 10);
			acciina2(3, 5, 3.6, 6.5, "Regar maceta", 0, 0, 5, 2, 1, 0, 0, 0, 0, 10);
			acciina2(4.2, 5.5, 7.2, 6.8, "Sofa", 5, 0, 2, 0, 0, 0, 0, 0, 0, 30);
			acciina2(8, 5.8, 9.5, 7.3, "Sofa", 5, 0, 2, 0, 0, 0, 0, 0, 0, 30);
			moviina(12.7, 4.2, 13.3, 6, "HouseGarden");
			moviina(13.7, 4.2, 15.8, 6, "Kitchen");
			acciina2(16.6, 5.6, 18.5, 7.4, "Sofa", 5, 0, 2, 0, 0, 0, 0, 0, 0, 30);
			moviina(17.7, 4.2, 18.3, 6, "Bedroom");
			acciina2(18.5, 5.2, 18.8, 6.2, "Regar maceta", 0, 0, 5, 2, 1, 0, 0, 0, 0, 10);
			break;
		case "HouseGarden":
			moviina(0.7, 4, 2, 5.5, "ForestPark");
			acciina2(3, 6, 4.5, 7, "Pecesitos", 0, 3, 5, 5, 5, 0, 0, 0, 0, 10);
			acciina2(4.5, 6.2, 6, 7.3, "Pecesitos", 0, 3, 5, 5, 5, 0, 0, 0, 0, 10);
			moviina(7.5, 4.1, 9, 6.2, "Hall");
			moviina(12.2, 4.2, 13.5, 5.5, "GreenHouse");
			acciina2(11.7, 6.5, 17.6, 9, "Arbusto", 0, 0, 5, 2, 5, 0, 0, 0, 0, 10);
			break;
		case "ForestPark":
			moviina(1.5, 4.3, 3.3, 5.4, "Beach");
			moviina(4.4, 4.3, 6.2, 5.3, "HouseGarden");
			moviina(8.8, 4, 9.9, 5.3, "Forest");
			moviina(14.8, 3.5, 16.3, 5.3, "City");
			acciina2(18.2, 5.3, 19.7, 6.2, "Park bank", 5, 0, 2, 2, 5, 0, 0, 0, 0, 30);
			break;
		case "Forest":
			moviina(1.5, 4.5, 2.5, 5.5, "ForestPark");
			break;
		case "Bedroom":
			acciina2(19.7, 4.5, 20, 8, "Closet", 0, 4, 20, 0, 0, 0, 8, 0, 0, 30);
			acciina2(0, 4.5, 1.5, 8, "Closet", 0, 4, 20, 0, 0, 0, 8, 0, 0, 30);
			moviina(1.5, 4.2, 2.5, 6.5, "Hall");
			if (CompGridEvent(3.4, 5.3, 7, 8))
				adistener("Dormir", 1, 0, 0, 0, 0, 0, 0, 0, 0, 8 * 60, 50);
			acciina2(8, 5.3, 9.5, 6.5, "Table", 1, 2, 2, 0, 0, 0, 0, 0, 0, 30);
			acciina2(12.8, 5.6, 13.7, 6.4, "PC_Desktop", -5, 0, 10, 10, 10, 0, -5, 0, 0, 30);
			moviina(15, 3.2, 16.7, 7.4, "Bathroom");
			break;
		case "Bathroom":
			acciina2(1.2, 5.6, 5.2, 7.8, "Bathtub", 0, 0, 0, 0, 0, 0, 33, 0, 0, 30);
			acciina2(3.3, 4, 4.2, 5.6, "Regar maceta", 0, 0, 5, 2, 1, 0, 0, 0, 0, 10);
			acciina2(1.2, 5.6, 5.2, 7.8, "Bathtub", 0, 0, 0, 0, 0, 0, 33, 0, 0, 30);
			moviina(7.4, 3.3, 8.8, 6.6, "Bedroom");
			acciina2(9, 3.5, 12, 6.2, "MakeUp", 0, 0, 0, 0, 0, 0, 0, 0, 0, 10000);
			if (CompGridEvent(9, 3.5, 12, 6.2))
				btnSiguiente.setVisible(true);
			acciina2(16, 3.5, 18.2, 7, "Shower", 0, 0, 0, 0, 0, 0, 66, 0, 0, 15);
			acciina2(18.8, 6, 19.8, 7, "Toilet", 0, 0, 0, 0, 0, -200, 0, 0, 0, 5);
			break;
		case "GreenHouse":
			acciina2(0.3, 4.8, 0.7, 5.5, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(0.7, 4.4, 1.3, 4.8, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(1.6, 4.5, 2.4, 5, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(1.8, 6.3, 2.2, 6.7, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(3.1, 5.0, 3.5, 5.5, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(7.7, 4.6, 9.4, 5.1, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(7.9, 5.2, 10.4, 6.0, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(9.7, 3.6, 10.6, 4.1, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(10.7, 4.5, 12.4, 5.5, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(12.4, 3.6, 13.3, 4.1, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(13.8, 4, 14.5, 6.5, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			moviina(14.5, 5, 15.7, 6.5, "HouseGarden");
			acciina2(16, 5.3, 17, 6.3, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(18.2, 4.2, 19, 4.6, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			acciina2(18.5, 4.9, 19.1, 6.1, "Invernadero", 0, 0, 5, 2, 3, 0, 0, 0, 0, 10);
			break;
		}
	}

	static void acciina(double varGX1, double varGY1, double varGX2, double varGY2, String varEvento, String varTipo) {
		if (CompGridEvent(varGX1, varGY1, varGX2, varGY2))
			JOptionPane.showMessageDialog(frame, varEvento, varTipo, 1);
	}

	void acciina2(double varGX1, double varGY1, double varGX2, double varGY2, String mximage, int sleepVar,
			int sweetVar, int sanityVar, int socialVar, int funVar, int toiletVar, int cleanVar, int hungerVar,
			int thirstVar, int mija) {
		if (CompGridEvent(varGX1, varGY1, varGX2, varGY2))
			adistener(mximage, sleepVar, sweetVar, sanityVar, socialVar, funVar, toiletVar, cleanVar, hungerVar,
					thirstVar, mija, 500);
	}

	void moviina(double varGX1, double varGY1, double varGX2, double varGY2, String varNewPlace) {
		if (CompGridEvent(varGX1, varGY1, varGX2, varGY2)) {
			placezar = varNewPlace;
			chargeImage();
		}
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
	}

	public void mouseReleased(MouseEvent arg0) {
	}
}